package br.edu.ifpb.sistemaestagio.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_ESTAGIO")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Estagio {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@OneToOne
	@JoinColumn(name="aluno_id")
	private Aluno aluno;
	
	@OneToOne
	@JoinColumn(name="empresa_id")
	private Empresa empresa;
	
	@Column(name = "ATIV_PRINC_OFERTA")
	private String atividadePrincipal;
	
	@Column(name = "CH_SEM_OFERTA")
	private int chSemanal;
	
	@Column(name = "VALOR_BOLSA_OFERTA")
	private float valorBolsa;
	
	@Column(name = "VALOR_TRANSP_OFERTA")
	private float valorTransporte;
	
	@Column(name = "VALOR_ALIM_OFERTA")
	private float valorAlimentacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DATA_INICIO")
	private Date dataInicio;
	
	@Column(name="ST_ESTAGIO")
	@Enumerated(EnumType.STRING) 
	private StatusEstagio status = StatusEstagio.PENDENTE_DE_APROVACAO;
	
	public Estagio(){}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getAtividadePrincipal() {
		return atividadePrincipal;
	}

	public void setAtividadePrincipal(String atividadePrincipal) {
		this.atividadePrincipal = atividadePrincipal;
	}

	public int getChSemanal() {
		return chSemanal;
	}

	public void setChSemanal(int chSemanal) {
		this.chSemanal = chSemanal;
	}

	public float getValorBolsa() {
		return valorBolsa;
	}

	public void setValorBolsa(float valorBolsa) {
		this.valorBolsa = valorBolsa;
	}

	public float getValorTransporte() {
		return valorTransporte;
	}

	public void setValorTransporte(float valorTransporte) {
		this.valorTransporte = valorTransporte;
	}

	public float getValorAlimentacao() {
		return valorAlimentacao;
	}

	public void setValorAlimentacao(float valorAlimentacao) {
		this.valorAlimentacao = valorAlimentacao;
	}

	public StatusEstagio getStatus() {
		return status;
	}

	public void setStatus(StatusEstagio status) {
		this.status = status;
	}

	

	
	
	
	
}
