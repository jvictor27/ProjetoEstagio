package br.edu.ifpb.sistemaestagio.facade;

import br.edu.ifpb.sistemaestagio.dao.PessoaDAO;
import br.edu.ifpb.sistemaestagio.entity.Pessoa;
import br.edu.ifpb.sistemaestagio.util.PasswordUtil;

public class LoginController {

	public Pessoa isValido(String login, String passwd) {
		
		PessoaDAO pdao = new PessoaDAO();
		Pessoa p = pdao.findByLogin(login);
		
		if(p != null && p.getSenha().equals(PasswordUtil.encryptMD5(passwd))){
			System.out.println("Senha: "+p.getSenha());
			return p;
		}
		return null;
	}

}
