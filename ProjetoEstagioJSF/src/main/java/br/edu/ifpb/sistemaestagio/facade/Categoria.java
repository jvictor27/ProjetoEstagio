package br.edu.ifpb.sistemaestagio.facade;

public enum Categoria {
	ERRO,
	INFO,
	SUCESSO,
	AVISO;

}
