package br.edu.ifpb.sistemaestagio.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.edu.ifpb.sistemaestagio.entity.Pessoa;

@ManagedBean (name="pessoaBean")
@SessionScoped
public class PessoaBean {
	private Pessoa pessoa;

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	
}
