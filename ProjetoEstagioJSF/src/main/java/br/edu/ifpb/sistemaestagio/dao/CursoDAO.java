package br.edu.ifpb.sistemaestagio.dao;

import javax.persistence.EntityManager;

import br.edu.ifpb.sistemaestagio.entity.Curso;

public class CursoDAO extends GenericDAO<Curso, Integer> {
	
	public CursoDAO() {
		this(PersistenceUtil.getCurrentEntityManager());
	}

	public CursoDAO(EntityManager em) {
		super(em);
	}
	
}
