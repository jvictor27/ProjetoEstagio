package br.edu.ifpb.sistemaestagio.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "TB_EMPRESA")
@DiscriminatorValue(value = "E")
public class Empresa extends Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "ATIVA_EMPRESA")
	private boolean ativa = true;
	
	@Column(name = "CNPJ_EMPRESA")
	private String cnpj;
	
	@Column(name = "NOME_EMPRESA")
	private String nome;
	
	@Column(name = "ATVPRIN_EMPRESA")
	private String atividadePrincipal;
	
	@Column(name = "SITE_EMPRESA")
	private String urlSite;
	
	@Column(name = "PESSOA_CONTATO_EMPRESA")
	private String pessoaContato;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="endereco_id")
	private Endereco endereco;
	
	@OneToMany(mappedBy = "empresa")
	private List<Oferta> ofertas;
	
	public Empresa(){}
	
	
	public List<Oferta> getOfertas() {
		return ofertas;
	}

	public void setOfertas(List<Oferta> ofertas) {
		this.ofertas = ofertas;
	}

	public void addOferta(Oferta oferta){
		if(!ofertas.contains(oferta)){
			ofertas.add(oferta);
		}
	}
	
	@Override
	public String toString() {
		return "Empresa [cnpj=" + cnpj + ", nome=" + nome + ", atividadePrincipal=" + atividadePrincipal
				+ ", getSenha()=" + getSenha() + "]";
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
	public String getPessoaContato() {
		return pessoaContato;
	}

	public void setPessoaContato(String pessoaContato) {
		this.pessoaContato = pessoaContato;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getAtividadePrincipal() {
		return atividadePrincipal;
	}

	public void setAtividadePrincipal(String atividadePrincipal) {
		this.atividadePrincipal = atividadePrincipal;
	}

	public String getUrlSite() {
		return urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	public boolean isAtiva() {
		return ativa;
	}


	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}

	
	

	

	

	
	
	
	
	
	
	
}
