package br.edu.ifpb.sistemaestagio.dao;

import javax.persistence.EntityManager;

import br.edu.ifpb.sistemaestagio.entity.Endereco;

public class EnderecoDAO extends GenericDAO<Endereco, Integer> {
	
	public EnderecoDAO() {
		this(PersistenceUtil.getCurrentEntityManager());
	}

	public EnderecoDAO(EntityManager em) {
		super(em);
	}


	
}
