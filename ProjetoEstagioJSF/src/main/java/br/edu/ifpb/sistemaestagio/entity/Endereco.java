package br.edu.ifpb.sistemaestagio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_ENDERECO")
public class Endereco {

	@Id
	@Column(name = "ID_ENDERECO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idEndereco;
	
	@Column(name = "PAIS_ENDERECO")
	private String pais;
	
	@Column(name = "ESTADO_ENDERECO")
	private String estado;
	
	@Column(name = "CIDADE_ENDERECO")
	private String cidade;
	
	@Column(name = "BAIRRO_ENDERECO")
	private String bairro;
	
	@Column(name = "RUA_ENDERECO")
	private String rua;
	
	@Column(name = "NUMERO_ENDERECO")
	private String numero;
	
	@Column(name = "CEP_ENDERECO")
	private String cep;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="empresa_id")
	private Empresa empresa;
	
	public Endereco(){}

	public Integer getIdEndereco() {
		return idEndereco;
	}

	public void setIdEndereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public String toString() {
		return "Endereco [idEndereco=" + idEndereco + ", pais=" + pais + ", estado=" + estado + ", cidade=" + cidade
				+ ", bairro=" + bairro + ", rua=" + rua + ", numero=" + numero + ", cep=" + cep + ", empresa=" + empresa
				+ "]";
	}

	
}
