package br.edu.ifpb.sistemaestagio.facade;

import br.edu.ifpb.sistemaestagio.dao.AlunoDAO;
import br.edu.ifpb.sistemaestagio.dao.CoordenadorDAO;
import br.edu.ifpb.sistemaestagio.dao.PersistenceUtil;
import br.edu.ifpb.sistemaestagio.entity.Aluno;
import br.edu.ifpb.sistemaestagio.entity.Coordenador;
import br.edu.ifpb.sistemaestagio.util.PasswordUtil;

public class CoordenadorController {
	
	public CoordenadorController(){}
	
	public boolean cadastrar(Coordenador coordenador) {
		CoordenadorDAO dao= new CoordenadorDAO(PersistenceUtil.getCurrentEntityManager());
		Coordenador c = dao.findByLogin(coordenador.getEmail());
		if(c == null){
			coordenador.setSenha( PasswordUtil.encryptMD5(coordenador.getSenha()));
			dao.beginTransaction();
			dao.insert(coordenador);
			dao.commit();
			return true;
		}
		return false;
		
	}

}
