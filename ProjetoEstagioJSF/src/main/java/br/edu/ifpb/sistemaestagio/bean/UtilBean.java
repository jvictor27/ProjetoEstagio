package br.edu.ifpb.sistemaestagio.bean;

import java.util.List;

import br.edu.ifpb.sistemaestagio.dao.PersistenceUtil;
import br.edu.ifpb.sistemaestagio.dao.PessoaDAO;
import br.edu.ifpb.sistemaestagio.entity.Pessoa;

public class UtilBean {
	public List<Pessoa> getPessoas() {
		PessoaDAO dao= new PessoaDAO(PersistenceUtil.getCurrentEntityManager());
		List<Pessoa> pessoas= dao.findAll();
		return pessoas;
		}
	}