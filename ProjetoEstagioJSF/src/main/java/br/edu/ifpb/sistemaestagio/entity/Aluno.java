package br.edu.ifpb.sistemaestagio.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TB_ALUNO")
@DiscriminatorValue(value = "A")
public class Aluno extends Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name="HAB_ALUNO")
	private String habilidades;
	
	@Column(name="CRE_ALUNO")
	private float cre;
	
	@Column(name="PERIODO_ALUNO")
	private int periodo;
	
	@ManyToOne
	@JoinColumn(name="curso_id", foreignKey = @ForeignKey(name = "fk_curso"))
	private Curso curso;
	
	@OneToMany(mappedBy="aluno")  
	private List<OfertaAluno> ofertaAlunos;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="estagio_id")
	private Estagio estagio;


	public Aluno(){ super(); }

	public Aluno(String habilidades, float cre, int periodo) {
		super();
		this.habilidades = habilidades;
		this.cre = cre;
		this.periodo = periodo;
	}
	
	public void addOfertaAluno(OfertaAluno ofertaAluno){
		if(ofertaAlunos == null){
			this.ofertaAlunos = new ArrayList<OfertaAluno>();
		}
		this.ofertaAlunos.add(ofertaAluno);
	}

	public List<OfertaAluno> getOfertaAlunos() {
		return ofertaAlunos;
	}

	public void setOfertaAlunos(List<OfertaAluno> ofertaAlunos) {
		this.ofertaAlunos = ofertaAlunos;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public String getHabilidades() {
		return habilidades;
	}

	public void setHabilidades(String habilidades) {
		this.habilidades = habilidades;
	}

	public float getCre() {
		return cre;
	}

	public void setCre(float cre) {
		this.cre = cre;
	}

	public int getPeriodo() {
		return periodo;
	}

	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}


	public Estagio getEstagio() {
		return estagio;
	}

	public void setEstagio(Estagio estagio) {
		this.estagio = estagio;
	}

	@Override
	public String toString() {
		return "Aluno [getHabilidades()=" + getHabilidades() + ", getCre()=" + getCre() + ", getPeriodo()="
				+ getPeriodo() + "]";
	}
	
	
}
