package br.edu.ifpb.sistemaestagio.entity;

public interface PessoaInterface {
	
	public void setSenha(String senha);
	
	public void setNome(String nome);
	public String getNome();	
	
	public void setTelefone(String telefone);
	public String getTelefone();
	

}
