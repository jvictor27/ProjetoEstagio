package br.edu.ifpb.sistemaestagio.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TB_OFERTA")
@Inheritance(strategy = InheritanceType.JOINED)
public class Oferta {

	@Id
	@Column(name = "ID_OFERTA")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "ATIV_PRINC_OFERTA")
	private String atividadePrincipal;
	
	@Column(name = "CH_SEM_OFERTA")
	private int chSemanal;
	
	@Column(name = "VALOR_BOLSA_OFERTA")
	private float valorBolsa;
	
	@Column(name = "VALOR_TRANSP_OFERTA")
	private float valorTransporte;
	
	@Column(name = "VALOR_ALIM_OFERTA")
	private float valorAlimentacao;
	
	@Column(name = "PRE_REQ_OFERTA")
	private String preRequisitos;
	
	@Column(name = "HAB_NEC_OFERTA")
	private String habilidadesTecnicasNecessarias;
	
	@Column(name = "HAB_TECN_DES_OFERTA")
	private String habilidadesTecnicasDesejadas;
	
	@Column(name="ST_OFERTA")
	@Enumerated(EnumType.STRING)
	private StatusOferta status = StatusOferta.PENDENTE_DE_APROVACAO;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="empresa_id")
	private Empresa empresa;
	
//	@ManyToMany(cascade = CascadeType.ALL)
//	@JoinTable(name="TB_OFERTA_ALUNO",
//    joinColumns={@JoinColumn(name="OFERTA_ID")},
//    inverseJoinColumns={@JoinColumn(name="ALUNO_ID")})
//	private List<Aluno> alunos;
	
	@OneToMany(mappedBy="oferta")
	private List<OfertaAluno> ofertaAlunos;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="curso_id")
	private Curso curso;
	
	
	public Oferta(){}
	
	public Oferta(String atividadePrincipal, int chSemanal, float valorBolsa, float valorTransporte,
			float valorAlimentacao, String preRequisitos, String habilidadesTecnicasNecessarias,
			String habilidadesTecnicasDesejadas, boolean statusAprovacao, boolean statusPreenhido, Empresa empresa,
			ArrayList<Aluno> alunos) {
		super();
		this.atividadePrincipal = atividadePrincipal;
		this.chSemanal = chSemanal;
		this.valorBolsa = valorBolsa;
		this.valorTransporte = valorTransporte;
		this.valorAlimentacao = valorAlimentacao;
		this.preRequisitos = preRequisitos;
		this.habilidadesTecnicasNecessarias = habilidadesTecnicasNecessarias;
		this.habilidadesTecnicasDesejadas = habilidadesTecnicasDesejadas;
		this.empresa = empresa;
	}
	
	
	
	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	public void setAlunos(List<Aluno> alunos) {
//		this.alunos = alunos;
//	}

	public String getAtividadePrincipal() {
		return atividadePrincipal;
	}
	public void setAtividadePrincipal(String atividadePrincipal) {
		this.atividadePrincipal = atividadePrincipal;
	}
	public int getChSemanal() {
		return chSemanal;
	}
	public void setChSemanal(int chSemanal) {
		this.chSemanal = chSemanal;
	}
	public float getValorBolsa() {
		return valorBolsa;
	}
	public void setValorBolsa(float valorBolsa) {
		this.valorBolsa = valorBolsa;
	}
	public float getValorTransporte() {
		return valorTransporte;
	}
	public void setValorTransporte(float valorTransporte) {
		this.valorTransporte = valorTransporte;
	}
	public float getValorAlimentacao() {
		return valorAlimentacao;
	}
	public void setValorAlimentacao(float valorAlimentacao) {
		this.valorAlimentacao = valorAlimentacao;
	}
	public String getPreRequisitos() {
		return preRequisitos;
	}
	public void setPreRequisitos(String preRequisitos) {
		this.preRequisitos = preRequisitos;
	}
	public String getHabilidadesTecnicasNecessarias() {
		return habilidadesTecnicasNecessarias;
	}
	public void setHabilidadesTecnicasNecessarias(String habilidadesTecnicasNecessarias) {
		this.habilidadesTecnicasNecessarias = habilidadesTecnicasNecessarias;
	}
	public String getHabilidadesTecnicasDesejadas() {
		return habilidadesTecnicasDesejadas;
	}
	public void setHabilidadesTecnicasDesejadas(String habilidadesTecnicasDesejadas) {
		this.habilidadesTecnicasDesejadas = habilidadesTecnicasDesejadas;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<OfertaAluno> getOfertaAlunos() {
		return ofertaAlunos;
	}

	public void setOfertaAlunos(List<OfertaAluno> ofertaAluno) {
		this.ofertaAlunos = ofertaAluno;
	}
	
	public void addOfertaAluno(OfertaAluno ofertaAluno){
		if(this.ofertaAlunos == null){
			this.ofertaAlunos = new ArrayList<OfertaAluno>();
		}
		this.ofertaAlunos.add(ofertaAluno);
	}

	public StatusOferta getStatus() {
		return status;
	}

	public void setStatus(StatusOferta status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Oferta [atividadePrincipal=" + atividadePrincipal + ", chSemanal=" + chSemanal + ", valorBolsa="
				+ valorBolsa + "]";
	}
	
	
}
