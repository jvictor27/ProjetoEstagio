package br.edu.ifpb.sistemaestagio;

import java.text.SimpleDateFormat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.edu.ifpb.sistemaestagio.dao.AlunoDAO;
import br.edu.ifpb.sistemaestagio.dao.CoordenadorDAO;
import br.edu.ifpb.sistemaestagio.dao.CursoDAO;
import br.edu.ifpb.sistemaestagio.dao.EmpresaDAO;
import br.edu.ifpb.sistemaestagio.dao.EnderecoDAO;
//import br.edu.ifpb.sistemaestagio.dao.ContatoDAO;
import br.edu.ifpb.sistemaestagio.dao.ManagedEMContext;
//import br.edu.ifpb.sistemaestagio.dao.OperadoraDAO;
import br.edu.ifpb.sistemaestagio.dao.PersistenceUtil;
import br.edu.ifpb.sistemaestagio.entity.Aluno;
import br.edu.ifpb.sistemaestagio.entity.Coordenador;
import br.edu.ifpb.sistemaestagio.entity.Curso;
import br.edu.ifpb.sistemaestagio.entity.Empresa;
import br.edu.ifpb.sistemaestagio.entity.Endereco;
//import br.edu.ifpb.sistemaestagio.dao.UsuarioDAO;
//import br.edu.ifpb.sistemaestagio.entity.Contato;
//import br.edu.ifpb.sistemaestagio.entity.Operadora;
//import br.edu.ifpb.sistemaestagio.entity.Perfil;
//import br.edu.ifpb.sistemaestagio.entity.Usuario;
import br.edu.ifpb.sistemaestagio.util.PasswordUtil;

public class Test1_Ins_EM_AL{
	private static EntityManagerFactory emf;
	private static SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	private EntityManager em;

	@BeforeClass
	public static void init() {
		PersistenceUtil.createEntityManagerFactory("estagio");
		emf = PersistenceUtil.getEntityManagerFactory();
		ManagedEMContext.bind(emf, emf.createEntityManager());
		System.out.println("init()");
	}

	@AfterClass
	public static void destroy() {
		if (emf != null) {
			emf.close();
			System.out.println("destroy()");
		}
	}

	@Before
	public void initEM() {
		em = emf.createEntityManager();
	}
	
	
	@Test
	public void test1(){
		try{
			Coordenador cod1= new Coordenador();
			cod1.setNome("Valéria");
			cod1.setEmail("valeria@ifpb.edu.br");
			cod1.setSenha(PasswordUtil.encryptMD5("senhaadm"));
			cod1.setTelefone("8396148795");

			
			Empresa emp1 = new Empresa();
			emp1.setNome("Softcom");
			emp1.setEmail("soft@ifpb.edu.br");
			emp1.setSenha(PasswordUtil.encryptMD5("senhasoftcom"));
			emp1.setCnpj("000145554");
			emp1.setUrlSite("www.softcom.com");
			emp1.setAtividadePrincipal("Programar");
			
			Empresa emp2 = new Empresa();
			emp2.setNome("Elfa");
			emp2.setEmail("elfa@ifpb.edu.br");
			emp2.setSenha(PasswordUtil.encryptMD5("senhaelfa"));
			emp2.setCnpj("00025585");
			emp2.setUrlSite("www.elfa.com");
			emp2.setAtividadePrincipal("Programar");
			
			Aluno al1 = new Aluno();
			al1.setNome("João Victor");
			al1.setEmail("joao@ifpb");
			al1.setCre(70);
			al1.setHabilidades("");
			al1.setPeriodo(5);
			al1.setSenha(PasswordUtil.encryptMD5("joaoaluno"));
			al1.setTelefone("232365656");
			
			Aluno al2 = new Aluno();
			al2.setNome("Vicente Correia");
			al2.setEmail("vicente@ifpb");
			al2.setCre(75);
			al2.setHabilidades("");
			al2.setPeriodo(5);
			al2.setSenha(PasswordUtil.encryptMD5("vicentealuno"));
			al2.setTelefone("456646546");
			
			Curso c1 = new Curso();
			c1.setNome("Sistemas para Internet");
			c1.setCh(30);
//			c1.setCoordenador(cod1);
			
//			cod1.setCurso(c1);
			
			System.out.println(c1.toString());
			CursoDAO cdao = new CursoDAO(em);
			cdao.beginTransaction();
			cdao.insert(c1);
			cdao.commit();
			
			Endereco end1 = new Endereco();
			end1.setRua("Rua fulano");
			end1.setNumero("301");
			end1.setBairro("Jaguaribe");
			end1.setCidade("João Pessoa");
			end1.setEstado("PB");
			end1.setCep("58000000");
			end1.setPais("Brasil");

			
			Endereco end2 = new Endereco();
			end2.setRua("Rua cicrano");
			end2.setNumero("305");
			end2.setBairro("Centro");
			end2.setCidade("João Pessoa");
			end2.setEstado("PB");
			end2.setCep("58333555");
			end2.setPais("Brasil");

			EmpresaDAO emp = new EmpresaDAO(em);
			emp.beginTransaction();
			emp.insert(emp1);
			emp.insert(emp2);
			emp.commit();
			
			EnderecoDAO enddao = new EnderecoDAO(em);
			enddao.beginTransaction();
			enddao.insert(end1);
			enddao.insert(end2);
			enddao.commit();
			
			CoordenadorDAO coddao= new CoordenadorDAO(em);
			coddao.beginTransaction();
			cod1=coddao.insert(cod1);
			coddao.commit();
			
			AlunoDAO aldao = new AlunoDAO(em);
			aldao.beginTransaction();
			al1 = aldao.insert(al1);
			al2 = aldao.insert(al2);
			aldao.commit();
			
			
//				
		}catch(Exception exc){
			Assert.fail("Erro de BD: " + exc.getMessage());
		}
	}
	
	
	@Test
	public void test2(){
		
		
		

		EmpresaDAO empdao = new EmpresaDAO(em);
		Empresa emp1 = empdao.find(1);
		Empresa emp2 = empdao.find(2);
		System.out.println(emp1.toString());
		System.out.println(emp2.toString());
		
		EnderecoDAO enddao = new EnderecoDAO(em);
		Endereco end1 = (Endereco) enddao.find(1);
		Endereco end2 = (Endereco) enddao.find(2);
		System.out.println("---- ENDEREÇOS:");
		System.out.println(end1.toString());
		System.out.println(end2.toString());
		
		emp1.setEndereco(end1);
		emp2.setEndereco(end2);
		
		end1.setEmpresa(emp1);
		end2.setEmpresa(emp2);
		
		
		enddao.beginTransaction();
		enddao.update(end1);
		enddao.update(end2);
		enddao.commit();
		
		empdao.beginTransaction();
		empdao.update(emp1);
		empdao.update(emp2);
		empdao.commit();
	}
	
	
	
	

}
