package br.edu.ifpb.sistemaestagio;

import java.text.SimpleDateFormat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.edu.ifpb.sistemaestagio.dao.AlunoDAO;
import br.edu.ifpb.sistemaestagio.dao.CoordenadorDAO;
import br.edu.ifpb.sistemaestagio.dao.CursoDAO;
import br.edu.ifpb.sistemaestagio.dao.EmpresaDAO;
//import br.edu.ifpb.sistemaestagio.dao.ContatoDAO;
import br.edu.ifpb.sistemaestagio.dao.ManagedEMContext;
import br.edu.ifpb.sistemaestagio.dao.OfertaAlunoDAO;
import br.edu.ifpb.sistemaestagio.dao.OfertaDAO;
//import br.edu.ifpb.sistemaestagio.dao.OperadoraDAO;
import br.edu.ifpb.sistemaestagio.dao.PersistenceUtil;
import br.edu.ifpb.sistemaestagio.entity.Aluno;
import br.edu.ifpb.sistemaestagio.entity.Coordenador;
import br.edu.ifpb.sistemaestagio.entity.Curso;
import br.edu.ifpb.sistemaestagio.entity.Empresa;
import br.edu.ifpb.sistemaestagio.entity.Oferta;
import br.edu.ifpb.sistemaestagio.entity.OfertaAluno;
import br.edu.ifpb.sistemaestagio.entity.StatusOferta;

public class Test2_Cri_Rel_OF {
	private static EntityManagerFactory emf;
	private static SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	private EntityManager em;

	@BeforeClass
	public static void init() {
		PersistenceUtil.createEntityManagerFactory("estagio");
		emf = PersistenceUtil.getEntityManagerFactory();
		ManagedEMContext.bind(emf, emf.createEntityManager());
		System.out.println("init()");
	}

	@AfterClass
	public static void destroy() {
		if (emf != null) {
			emf.close();
			System.out.println("destroy()");
		}
	}

	@Before
	public void initEM() {
		em = emf.createEntityManager();
	}
	
	@Test
	public void test1(){
    
    CoordenadorDAO coddao= new CoordenadorDAO(em);	
		Coordenador cod1= coddao.find(3);
    
		EmpresaDAO empdao = new EmpresaDAO(em);
		Empresa emp1 = empdao.find(1);
		
		AlunoDAO aldao = new AlunoDAO(em);
		Aluno al1 = aldao.find(4);
		Aluno al2 = aldao.find(5);
		
		CursoDAO cdao = new CursoDAO(em);
		Curso c1 = cdao.find(1);
		
		Oferta of1 = new Oferta();
		of1.setAtividadePrincipal("Programa a dor");
		of1.setChSemanal(20);
		of1.setHabilidadesTecnicasDesejadas("Profissional capaz de programar... a dor.");
		of1.setHabilidadesTecnicasNecessarias("Programá");
		of1.setPreRequisitos("Saber o que é uma variável");
		of1.setValorAlimentacao(150);
		of1.setValorBolsa(200);
		of1.setValorTransporte(100);
		of1.setEmpresa(emp1);
//		of1.addAluno(al1);
//		of1.addAluno(al2);
		of1.setCurso(c1);
		
		
		
		Oferta of2 = new Oferta();
		of2.setAtividadePrincipal("front end");
		of2.setChSemanal(20);
		of2.setHabilidadesTecnicasDesejadas("Profissional capaz de programar front end.");
		of2.setHabilidadesTecnicasNecessarias("Programá front end");
		of2.setPreRequisitos("Saber o que é front end");
		of2.setValorAlimentacao(150);
		of2.setValorBolsa(200);
		of2.setValorTransporte(100);
		of2.setEmpresa(emp1);
//		of2.addAluno(al1);
//		of2.addAluno(al2);
		of2.setCurso(c1);
		of2.setStatus(StatusOferta.APROVADO);
		
		Oferta of3 = new Oferta();
		of3.setAtividadePrincipal("design");
		of3.setChSemanal(20);
		of3.setHabilidadesTecnicasDesejadas("Profissional capaz de programar design.");
		of3.setHabilidadesTecnicasNecessarias("Programá design");
		of3.setPreRequisitos("Saber o que é design");
		of3.setValorAlimentacao(150);
		of3.setValorBolsa(200);
		of3.setValorTransporte(100);
		of3.setEmpresa(emp1);
//		of3.addAluno(al1);
//		of3.addAluno(al2);
		of3.setCurso(c1);
		of3.setStatus(StatusOferta.APROVADO);
		
		OfertaAluno ofal1 = new OfertaAluno();
		ofal1.setOferta(of1);
		ofal1.setAluno(al1);
		of1.addOfertaAluno(ofal1);
		
		OfertaAluno ofal2 = new OfertaAluno();
		ofal2.setOferta(of1);
		ofal2.setAluno(al2);
		of1.addOfertaAluno(ofal2);
		
		c1.addOferta(of1);
		c1.addOferta(of2);
		c1.addOferta(of3);
		
		
		//Set os 2 alunos no curso TSI
		al1.setCurso(c1);
		al2.setCurso(c1);
    
		//Set os curso ao Coordenador
		cod1.setCurso(c1);
		c1.setCoordenador(cod1);
		
		//Add Oferta em empresa
		emp1.addOferta(of1);
		emp1.addOferta(of2);
		emp1.addOferta(of3);
		
		OfertaDAO ofdao = new OfertaDAO(em);
		ofdao.beginTransaction();
		ofdao.insert(of1);
		ofdao.insert(of2);
		ofdao.insert(of3);
		ofdao.commit();
		
		OfertaAlunoDAO ofaldao = new OfertaAlunoDAO(em);
		ofaldao.beginTransaction();
		ofaldao.insert(ofal1);
		ofaldao.insert(ofal2);
		ofaldao.commit();
    
		coddao.beginTransaction();
		coddao.update(cod1);
		coddao.commit();
		
		aldao.beginTransaction();
		aldao.update(al1);
		aldao.update(al2);
		aldao.commit();
		
		cdao.beginTransaction();
		cdao.update(c1);
		cdao.commit();
		
		empdao.beginTransaction();
		empdao.update(emp1);
		empdao.commit();
		
	}
	
	
	

}

